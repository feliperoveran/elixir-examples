# implementing struct, a limited form of map

defmodule Attendee do
  defstruct name: "", paid: false, over_18: true

  def may_attend_after_party(attendee = %Attendee{}) do
    attendee.paid && attendee.over_18
  end

  def print_vip_badge(%Attendee{name: name}) when name !=  "" do
    IO.puts "Very cheap badge for #{name}"
  end

  def print_vip_badge(%Attendee{}) do
    raise "missing name for badge"
  end
end

# a1 = %Attendee{ name: 'Felipe', paid: true, over_18: true }
# Attendee.may_attend_after_party(a1)
# => true
#
# Attendee.print_vip_badge(a1)
# => Very cheap badge for Felipe
#
# a2 = %Attendee{}
# Attendee.print_vip_badge(a2)
# => ** (RuntimeError) missing name for badge
#     maps/struct.exs:15: Attendee.print_vip_badge/1
