###############
# Functions-2 #
###############
# Pattern matching on anonymous functions
args = fn
  (0, 0, _) -> "FizzBuzz"
  (0, _, _) -> "Fizz"
  (_, 0, _) -> "Buzz"
  (_, _, a) -> a
end

IO.puts args.(0, 0, 3)
IO.puts args.(0, 1, 2)
IO.puts args.(1, 2, 10)

###############
# Functions-3 #
###############
remainder = fn
  (n) -> args.(rem(n, 3), rem(n, 5), n)
end

# Call remainder with n from 10 to 16
Enum.map(10..16, &(IO.puts(remainder.(&1))))

###############
# Functions-4 #
###############

# Functions that return functions
prefix = fn str1 -> (fn str2 -> str1 <> " " <> str2 end) end

mrs = prefix.("Mrs")
IO.puts mrs.("Smith")

IO.puts prefix.("Elixir").("Rocks")

###############
# Functions-5 #
###############
# Rewrite using & notation

Enum.map([1, 2, 3, 4], fn x -> x + 2 end)
Enum.map([1, 2, 3, 4], &(&1 + 2))

Enum.each [1,2,3,4], fn x -> IO.inspect x end
Enum.each [1, 2, 3, 4], &(IO.inspect &1)
