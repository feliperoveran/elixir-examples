defmodule Times do
  def double(n), do: n * 2

  def quadruple(n) do
    double(n) * 2
  end
end

defmodule Factorial do
  def of(0), do: 1
  def of(n) when n > 0 do # using a guard clause so it does not loop forever
    n * of(n-1)
  end
end

defmodule Sum do
  def calculate(0), do: 0
  def calculate(n), do: n + calculate(n-1)
end

defmodule Divisors do
  def gcd(x, 0), do: x
  def gcd(x, y), do: gcd(y, rem(x, y))
end

##############
# Exercise 6 #
##############
defmodule Chop do
  def guess(actual, first..last) do
    current_guess = div(first + last, 2)

    IO.puts "Is it #{current_guess}?"

    _guess(actual, current_guess, first..last)
  end

  defp _guess(actual, current_guess, first.._last) when current_guess > actual do
    guess(actual, first..current_guess - 1)
  end

  defp _guess(actual, current_guess, _first..last) when current_guess < actual do
    guess(actual, (current_guess + 1)..last)
  end

  defp _guess(actual, actual, _) do
    IO.puts "Yes its #{actual}"
  end
end
