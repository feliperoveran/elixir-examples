defmodule Lists do
  # len([11,12,13,14,15]
  # = 1 + len([12,13,14,15])
  # = 1 + 1 + len([13,14,15])
  # = 1 + 1 + 1 + len([14,15]
  # = 1 + 1 + 1 + 1 + len([15])
  # = 1 + 1 + 1 + 1 + 1 + len([])
  # = 1 + 1 + 1 + 1 + 1 + 0
  # = 5
  def len([]), do: 0
  def len([_head | tail]), do: 1 + len(tail)

#######
# sum #
#######
  def sum(list), do: _sum(list, 0)

  defp _sum([], total), do: total
  defp _sum([head | tail], total), do: _sum(tail, head + total)

##############
# Exercise 0 #
##############

  def sum_no_acc([head | tail]) do
    head + sum_no_acc(tail)
  end

  def sum_no_acc([]), do: 0

##########
# Reduce #
##########

  def reduce([], value, _), do: value
  def reduce([head | tail], value, fun) do
    reduce(tail, fun.(head, value), fun)
  end

##############
# Exercise 1 #
##############
# Applies the function to each element and then returns the sum

  def mapsum([],  _), do: 0
  def mapsum([head | tail], func) do
    func.(head) + mapsum(tail, func)
  end

##############
# Exercise 2 #
##############

  def max([x]), do: x
  def max([head | tail]) do
    Kernel.max(head, max(tail))
  end

##############
# Exercise 3 #
##############

  def caesar([], _n), do: []
  def caesar([head | tail], n)
    when head + n <= ?z,
    do: [head + n | caesar(tail, n)]

  def caesar([head | tail], n),
    do: [head + n - 26 | caesar(tail, n)]

#########################
# Swap pair of elements #
#########################

  def swap([]), do: []
  def swap([_]), do: raise "Can't swap a list with an odd number of elements"
  def swap([a, b | tail]), do: [b, a | swap(tail)]

############
# Exercise 4
############
# returns a list of elements from `from` up to `to`

  # from = 1, to = 5
  # [ 1 | span(2, 5)]
  # [ 1 | [ 2 | span(3, 5)] ]
  # [ 1 | [ 2 | [ 3 | span(4, 5) ] ] ]
  # [ 1 | [ 2 | [ 3 | [ 4 | span(5, 5) ] ] ] ]
  # [ 1 | [ 2 | [ 3 | [ 4 | [ 5 | span(6, 5) ] ] ] ] -> from > to, so []
  # [ 1 | [ 2 | [ 3 | [ 4 | [ 5 | [] ] ] ] ]
  # [1, 2, 3, 4, 5]
  def span(from, to) when from > to, do: []
  def span(from, to) do
    [from | span(from + 1, to)]
  end

##############
# Exercise 5 #
##############
# Implement Enum functions #all?, #each, #filter, #split and #take

  def all?([]), do: true
  def all?([head | tail]) do
    head && all?(tail)
  end

  def each([], _), do: []
  def each([ head | tail ], func) do
    [ func.(head) | each(tail, func) ]
  end

  def filter([], _), do: []
  def filter([ head | tail ], func) do
    if func.(head) do
      [ head | filter(tail, func) ]
    else
      filter(tail, func)
    end
  end

  def split([], _), do: {[], []}

##############
# Exercise 6 #
##############

  def flatten([]), do: []
  def flatten([ head | tail ]), do: flatten(head) ++ flatten(tail)
  def flatten(head), do: [ head ]
end
